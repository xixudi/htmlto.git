
# htmlto

#### 介绍
爬取网站html相关资源并转成其他格式,如chm/pdf等

- 使用jsoup爬取doc网页资源
- 使用enjoy模板生成chm的工程配置文件
- 代码简单,欢迎大家使用 
 
##### 放两张生成的chm和pdf的效果图
![pdf](https://images.gitee.com/uploads/images/2019/0718/154345_2e655bff_911872.png "pdf.png")
![chm](https://images.gitee.com/uploads/images/2019/0718/154327_907578e2_911872.png "chm.png")

---
##### 20190903 新增罗辑思维的文章
1. 爬了[罗辑集](http://www.luojiji.com/)的[罗辑思维](http://www.luojiji.com/forum-LJSWW-1.html)系列文章

---
##### 20190715 适应新版[jfinal官网](https://www.jfinal.com/)
1. 样式适应新版jfinal官网
2. jfinal-doc更新到4.3

---
##### 20190221 新增jfinal doc 的pdf版本
1. 拼接所有chm用的html到单个的all.html
2. 使用wkhtmltopdf把all.html转成pdf
- *尝试使用itext,但是对h5和css支持不好,放弃了*

---
##### 20190220 新增jfinal-weixin的wiki doc
- 因为没有图片什么的,所以很简单
- 其中1.1和1.4wiki上没找到,先404了
- 样式已从wiki上下载,并因为chm兼容问题做了部分修改
- 尝试代码高亮,但因为ie8,又没想用htmlunit爬取异步内容,所以暂搁浅了
- 运行JifinalWeixinDoc.main()即可

---
##### 20190219
  目前仅有根据jfinal官网的doc生成chm文档的部分,运行JfinalDoc.main()即可


