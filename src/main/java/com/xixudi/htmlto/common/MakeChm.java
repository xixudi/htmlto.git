package com.xixudi.htmlto.common;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Template;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * 根据模板生成chm工程文件,继而生成chm
 * @Author YangZheng 328170112@qq.com
 * @Date 2019-02-18 10:25
 */
public class MakeChm {
    private String projectName;
    private String basePath;
    Kv kv;
    MakeChm(String projectName,ArrayList<ContentsNode> nodes,String indexPage,String title){
        this.projectName = projectName;
        this.basePath=SomeKit.getPath(projectName);
        if(StrKit.isBlank(indexPage)) indexPage = getFirstPage(nodes);
        if(StrKit.isBlank(title)) title = projectName+"帮助文档";
        this.kv = Kv.by("nodes", ContentsNode.makeTreeData(nodes)).set("projectName",projectName).set("index","source\\"+indexPage).set("title",title);
    }
    public void make() {
        writePropFile("hhc");
        writePropFile("hhk");
        writePropFile("hhp");
        makeChm();
    }
    public void makeIndexPage(){
        writePropFile("index.html","source/index.html","utf8");
        ArrayList<ContentsNode> nodes = (ArrayList<ContentsNode>)kv.get("nodes");
        nodes.add(0,ContentsNode.newTopic("速查表","index.html") );
    }
    private void makeChm() {
        String command = "\""+SomeKit.getPath("chm")+"/hhc.exe\" "+ projectName +".hhp";
        CommandHelper.run(command,null,basePath);
    }

    private void writePropFile(String suffix){
        writePropFile("main."+suffix, projectName +"."+suffix,"gbk");
    }
    private void writePropFile(String oldName,String newName,String charset){
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(basePath+"/"+newName), charset));
            Template template = SomeKit.getTemplate("chm",oldName);
            template.render(kv, output);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static String getFirstPage(ArrayList<ContentsNode> nodes){
        for (ContentsNode node : nodes) {
            if(StrKit.isBlank(node.getPath())) {
                if(node.getChildren()!=null&&node.getChildren().size()>0) {
                    return getFirstPage(node.getChildren());
                } else {
                    return "";
                }
            } else {
                return node.getPath();
            }
        }
        return "";
    }
    public static void createChm(String name,ArrayList<ContentsNode> nodes,String indexPage,String title,boolean needMakeIndexPage){
        MakeChm makeChm = new MakeChm(name,nodes,indexPage,title);
        if(needMakeIndexPage) makeChm.makeIndexPage();
        makeChm.make();
    }
}
