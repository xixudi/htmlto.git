package com.xixudi.htmlto.common;

/**
 * @desc 使用wkhtmltopdf 把html转为pdf
 * @author YangZheng 328170112@qq.com
 * @date 2019-02-21 19:49
 */
public class MakePDF {
    public static void htmlToPdf(String htmlPath, String newPdfPath) {
        String opts = "--outline --outline-depth 2" +//生成目录,深度2
                " --margin-top 5mm --margin-right 5mm --margin-bottom 5mm --margin-left 5mm";//边距
        String command = "\"" + SomeKit.getPath("pdf") + "/wkhtmltopdf.exe\" "
                        + opts + " \"" + htmlPath + "\" \"" + newPdfPath + "\"";
        CommandHelper.run(command,null,null);
    }
}