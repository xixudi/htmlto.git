package com.xixudi.htmlto.common;

import com.jfinal.template.Engine;
import com.jfinal.template.Template;

import java.util.HashMap;
import java.util.Map;

/**
 * 剥离几个多处使用的方法
 * @author YangZheng 328170112@qq.com
 * @date 2019-09-03 11:07
 */
public class SomeKit {
    private static final String ROOT_PATH=SomeKit.class.getClassLoader().getResource("").getPath().substring(1);
    private static final Engine engine = Engine.use().setBaseTemplatePath(ROOT_PATH);
    public static String getPath(String projectName) {
        return ROOT_PATH + projectName;
    }
    public static Template getTemplate(String projectName,String name) {
        return engine.getTemplate(projectName+'/'+name);
    }
    public static String renderTemplate(String projectName,String name, Map<?,?> data) {
        return engine.getTemplate(projectName+'/'+name).renderToString(data);
    }
    public static String renderTemplate(String projectName,String name, String data) {
        HashMap<String, String> map = new HashMap<>();
        map.put("data",data);
        return engine.getTemplate(projectName+'/'+name).renderToString(map);
    }
}
